{
  const {
    html,
  } = Polymer;
  /**
    `<cells-jhony-login>` Description.

    Example:

    ```html
    <cells-jhony-login></cells-jhony-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-jhony-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsJhonyLogin extends Polymer.Element {

    static get is() {
      return 'cells-jhony-login';
    }

    static get properties() {
      return {
        name1: String,
        psw1: String,
        login: {
          type: Boolean,
          value: false,
          notify: true
        }
      };
    }
    
    go() {
      if (this.name1 === 'jhony' && this.psw1 === '1234') {
        this.dispatchEvent(new CustomEvent('Login Succes', { detail: this.name1 }));
        console.log('SUCCES');
      } else {
        this.dispatchEvent(new CustomEvent('Login Error', { detail: this.name1}));
        console.log('ERROR');
      }
    }

    static get template() {
      return html`
      <style include="cells-jhony-login-styles cells-jhony-login-shared-styles"></style>

      <slot></slot>
      <br><br><center><h4>¡¡Bienvenido!!</h4></center>
      <div class="imgcontainer">
        <img src="../imagen/avatar.png" alt="Avatar" class="avatar">
      </div>
          <div>
          <label for="uname"><b>Username : </b></label>
          <input type="text" placeholder="Enter Username" name="name1" value="{{name1::input}}" required><br>
          <label for="psw"><b>Password : </b></label>
          <input type="password" placeholder="Enter Password" name="psw1" value="{{psw1::input}}" required><br>
          <button on-click="go">LOGIN</button>
          </div>      
      `;
    }
  }

  customElements.define(CellsJhonyLogin.is, CellsJhonyLogin);
}